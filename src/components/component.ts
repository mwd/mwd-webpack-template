import * as MwdLogo  from '../assets/mwd-logo.svg';

console.log(MwdLogo);

export default function component() {
    document.body.innerHTML = `
<div class="content">
  <h1>Hello mwd template</h1>
  <div class="logo">${MwdLogo}</div>
  <p class="copy">a simple webapp boilerplate with typescript and scss</p>
  <p class="copy">
    <a href="https://gitlab.com/mwd-dev/mwd-webpack-template/" target="_blank"
      >https://gitlab.com/mwd-dev/mwd-webpack-template</a
    >
  </p>
</div>
`;
}
